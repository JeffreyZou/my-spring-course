package com.citi.training;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestVehicle {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(VehicleConfig.class);
        Person p = context.getBean(Person.class);
        p.getPet().feed();
        p.getVehicle().drive();
    }
}
