package com.citi.training;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VehicleConfig {

    @Bean
    public Person fanng() {
        Person p = new Person();
        p.setPet(jessica());
        p.setVehicle(lightningMcQueen());
        p.setName("Fanng");
        return p;
    }

    @Bean
    public Pet jessica() {
        return new Cat();
    }

    @Bean
    public Pet doggo() {
        return new Dog();
    }

    @Bean
    public Vehicle lightningMcQueen() {
        return new Car();
    }

    @Bean
    public Vehicle boatyMcBoatFace() {
        return new Boat();
    }
}
