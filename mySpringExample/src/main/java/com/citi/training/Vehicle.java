package com.citi.training;

public interface Vehicle {

    void drive();
}
