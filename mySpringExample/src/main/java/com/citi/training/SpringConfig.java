package com.citi.training;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {

    @Bean
    public Person bob() {
        Person p = new Person();
        p.setPet(jessica());
        return p;
        // return new Person();
    }

    @Bean
    public Pet jessica() {
        return new Cat();
    }

    @Bean
    public Pet doggo() {
        return new Dog();
    }
}
