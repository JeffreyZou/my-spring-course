package com.citi.training;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        // ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
//        Person p = context.getBean(Person.class, "person");
//        p.getPet().feed();
//        System.out.println(p.getName());
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(Person.class).getPet().feed();
    }
}
