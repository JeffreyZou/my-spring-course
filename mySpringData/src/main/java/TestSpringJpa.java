import com.conygre.spring.entities.CompactDisc;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.conygre.spring.configuration.AppConfig;
import com.conygre.spring.service.CompactDiscService;

import java.util.List;


public class TestSpringJpa {

	public static void main(String[] args) {
		
		ApplicationContext context =
				new  AnnotationConfigApplicationContext(AppConfig.class);
		CompactDiscService service = context.getBean(CompactDiscService.class);
		
		service.getCatalog().forEach(c -> System.out.println(c.getTitle()));
		
		List<CompactDisc> cdsByArtist = service.getCompactDiscByArtists("Coldplay");
		System.out.println("~~~~~~~~~~ CDs by Coldplay ~~~~~~~~~~~");
		cdsByArtist.forEach(c -> System.out.println(c.getTitle()));
		List<CompactDisc> cdsByPrice = service.getCompactDiscByPrice(13.0);
		System.out.println("~~~~~~~~~~ CDs less than $13 ~~~~~~~~~~~");
		cdsByPrice.forEach(c -> System.out.println(c.getTitle()));
	}

}
