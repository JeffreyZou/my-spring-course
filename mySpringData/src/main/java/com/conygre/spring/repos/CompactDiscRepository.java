package com.conygre.spring.repos;

import com.conygre.spring.entities.CompactDisc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompactDiscRepository extends JpaRepository<CompactDisc, Integer> {
    List<CompactDisc> findByArtist(String artist);
    List<CompactDisc> findByPriceLessThanEqual(Double price);
}
