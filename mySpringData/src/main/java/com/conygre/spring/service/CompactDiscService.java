package com.conygre.spring.service;

import com.conygre.spring.dao.CompactDiscDAO;
import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.repos.CompactDiscRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@Transactional (propagation = Propagation.REQUIRED)
public class CompactDiscService {
	
	@Autowired
	private CompactDiscRepository repository;
	
	@Autowired
	public void setDao(CompactDiscRepository repository) {
		this.repository = repository;
	}
	@Transactional (propagation = Propagation.REQUIRES_NEW)
	public void addToCatalog(CompactDisc disc) {
			repository.save(disc);
	}
	
	public List<CompactDisc> getCatalog() {
		return repository.findAll();
	}

	public CompactDisc getCompactDiscById(int id) {
		return repository.findById(id).orElse(null);
	}

	public List<CompactDisc> getCompactDiscByArtists(String artist) {
		return repository.findByArtist(artist);
	}

	public List<CompactDisc> getCompactDiscByPrice(Double price) {
		return repository.findByPriceLessThanEqual(price);
	}
}
