package com.citi.spring.boot.services;

import com.citi.training.entities.CompactDisc;

public interface CompactDiscService {
	Iterable<CompactDisc> getCatalog();
}