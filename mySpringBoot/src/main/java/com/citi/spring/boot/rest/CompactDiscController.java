package com.citi.spring.boot.rest;

import com.citi.spring.boot.services.CompactDiscService;
import com.citi.training.entities.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/compactdiscs")
public class CompactDiscController {
    @Autowired
    private CompactDiscService service;
    @RequestMapping(method = RequestMethod.GET)
    Iterable<CompactDisc> findAll() {
        return service.getCatalog();
    }
}
