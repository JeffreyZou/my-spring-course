package com.citi.spring.boot.services;

import com.citi.spring.boot.repos.CompactDiscRepository;
import com.citi.training.entities.CompactDisc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompactDiscServiceImpl implements CompactDiscService {
    @Autowired
    private CompactDiscRepository dao;
    /* (non-Javadoc)
     * @see com.conygre.spring.boot.services.ICompactDiscService#getCatalog()
     */
    public Iterable<CompactDisc> getCatalog() {
        return dao.findAll();
    }
}