import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.assertEquals;

public class TestSpeakingClock {

    // Fixtures
    SpeakingClock clock;

    @Before
    public void setup() {
        clock = new SpeakingClock();
    }

    @Test
    public void canDoMidnight() {
        // 1. Arrange
        LocalTime midnight = LocalTime.MIDNIGHT;
        // 2. Act
        String result = clock.timeToText(midnight);
        // 3. Assert
        assertEquals("midnight", result);
    }

    @Test
    public void canDoMidday() {
        // 1. Arrange
        LocalTime noon = LocalTime.NOON;
        // 2. Act
        String result = clock.timeToText(noon);
        // 3. Assert
        assertEquals("midday", result);
    }

    @Test
    public void canDo15() {
        // 1. Arrange
        LocalTime time = LocalTime.of(15,0);
        // 2. Act
        String result = clock.timeToText(time);
        // 3. Assert
        assertEquals("three o'clock", result);
    }

    @Test
    public void canDo1530() {
        // 1. Arrange
        LocalTime time = LocalTime.of(15,30);
        // 2. Act
        String result = clock.timeToText(time);
        // 3. Assert
        assertEquals("half past three", result);
    }
}
