import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class TestStrings {

    // Fixtures
    String literalString1;
    String literalString2;
    String newString;

    @Before
    public void setup() {
        literalString1 = "hello";
        literalString2 = "hello";
        newString = new String("hello");
    }

    @Test
    public void testThatDoubleEqualsWorksWithStringLiterals() {
        assertTrue(literalString1==literalString2);
    }

    @Test
    public void testThatComparingNewStringsWithDoubleEqualsFails() {
        assertFalse(literalString1==newString);
    }
}
