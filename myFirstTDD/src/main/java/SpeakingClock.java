import java.time.LocalTime;

public class SpeakingClock {
    public String timeToText(LocalTime time) {
        int hour = time.getHour();
        int minute = time.getMinute();
        String[] times = {"midnight", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                "ten", "eleven", "noon", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven"};
        if (hour == 0 && minute == 0)
            return "midnight";
        else if (hour == 12 && minute == 0)
            return "midday";
        else if (minute == 0)
            return times[hour] + " o'clock";
        else if (minute == 5)
            return "five past " + times[hour];
        else if (minute == 10)
            return "ten past " + times[hour];
        else if (minute == 15)
            return "a quarter past " + hour;
        else if (minute == 20)
            return "twenty past " + times[hour];
        else if (minute == 25)
            return "twenty five past " + times[hour];
        else if (minute == 30)
            return "half past " + times[hour];
        else if (minute == 35)
            return "twenty five to " + times[hour];
        else if (minute == 40)
            return "twenty to " + times[hour];
        else if (minute == 45)
            return "a quarter to" + times[(hour+1)%24];
        else if (minute == 50)
            return "ten to " + times[hour];
        else if (minute == 55)
            return "five to " + times[hour];
        else if (minute < 5) {
            return "around five past " + times[hour];
        }
        else if (minute < 10) {
            return "around ten past " + times[hour];
        }
        else if (minute < 15) {
            return "around quarter past " + times[hour];
        }
        else if (minute < 20) {
            return "around twenty past " + times[hour];
        }
        else if (minute < 25) {
            return "around twenty five past" + times[hour];
        }
        else if (minute < 30) {
            return "around half past" + times[hour];
        }
        else if (minute < 35) {
            return "around twenty five to " + times[(hour+1)%24];
        }
        else if (minute < 40) {
            return "around twenty to " + times[(hour+1)%24];
        }
        else if (minute < 45) {
            return "around quarter to " + times[(hour+1)%24];
        }
        else if (minute < 50) {
            return "around ten to " + times[(hour+1)%24];
        }
        else if (minute < 55) {
            return "around five to " + times[(hour+1)%24];
        }
        else return null;
    }
}
