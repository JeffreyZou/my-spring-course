package com.citi.training.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="tracks")
public class Track implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title") private String title;
    @JoinColumn(name="cd_id", referencedColumnName = "id", nullable = false)
    @ManyToOne
    private CompactDisc disc;

    public Track(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CompactDisc getDisc() {
        return disc;
    }

    public void setDisc(CompactDisc disc) {
        this.disc = disc;
    }
}
