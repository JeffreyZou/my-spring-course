package com.citi.training.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="compact_discs")
public class CompactDisc implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    @Column(name="title") private String title;
    @Column(name="artist") private String artist;
    @Column(name="price") private Double price;
    @OneToMany(mappedBy = "disc", cascade={CascadeType.MERGE, CascadeType.PERSIST})
    private List<Track> trackTitles = new ArrayList<>();

    public CompactDisc() {
    }

    public CompactDisc(String title, String artist, Double price) {
        this.title = title;
        this.artist = artist;
        this.price = price;
    }

    public List<Track> getTrackTitles() {
        return trackTitles;
    }

    public void setTrackTitles(List<Track> trackTitles) {
        this.trackTitles = trackTitles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void addTrack(Track t) {
        t.setDisc(this);
        trackTitles.add(t);
    }
}
