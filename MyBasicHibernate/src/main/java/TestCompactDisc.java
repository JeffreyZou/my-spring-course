import com.citi.training.entities.CompactDisc;
import com.citi.training.entities.Track;

import javax.persistence.*;
import java.util.List;

public class TestCompactDisc {

    public static void main(String[] args) {
        EntityManagerFactory factory =
                Persistence.createEntityManagerFactory("conygrePersistenceUnit");
        EntityManager em = factory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        // Query q = em.createQuery("from CompactDisc where title = 'White Ladder'");
        // List<CompactDisc> discs = q.getResultList();
        // discs.forEach((disc) -> System.out.println(disc.getArtist()));

        // Getting all Spice Girl Tracks
        // Query allSpiceGirlTracks = em.createQuery("select t.title from Track t where t.disc.id = 16");
        // List<String> spiceTitles = allSpiceGirlTracks.getResultList();
        // spiceTitles.forEach(System.out::println);

        // Inserting a new Album
        /*
        CompactDisc myCD = new CompactDisc("Help", "Beatles", 8.99);
        myCD.addTrack(new Track("Help"));
        myCD.addTrack(new Track("Eleanor Rigby"));
        em.persist(myCD);*/



        tx.commit();
        em.close();
        factory.close();
    }
}
